package com.manny.firstlab.Controller

import com.manny.firstlab.Controller.Errors.*
import com.manny.firstlab.Main
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import javafx.scene.control.*
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.util.Duration
import kotlinx.coroutines.*
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

enum class Errors {
    CANCEL, OK, ZERO, BIG_STEP
}

class MainController {

    @FXML
    lateinit var graphPane: AnchorPane
    @FXML
    private lateinit var mainPane: AnchorPane
    @FXML
    lateinit var xFromField: TextField
    @FXML
    lateinit var xToField: TextField
    @FXML
    lateinit var aField: TextField
    @FXML
    lateinit var cField: TextField
    @FXML
    lateinit var stepField: TextField
    @FXML
    private lateinit var coordinatesView: TableView<CoordinatesTable>
    @FXML
    private lateinit var xColumn: TableColumn<CoordinatesTable, Double>
    @FXML
    private lateinit var y1Column: TableColumn<CoordinatesTable, Double>
    @FXML
    private lateinit var y2Column: TableColumn<CoordinatesTable, Double>

    private var yAxis: NumberAxis? = null
    private var xAxis: NumberAxis? = null
    private var chart2dTemp: LineChart<Number, Number>? = null
    var coordList: ObservableList<CoordinatesTable>? = null

    fun initialize() {

        setPattern(xFromField, "-?(\\d+\\.?\\d*)?")
        setPattern(xToField, "-?(\\d+\\.?\\d*)?")
        setPattern(aField, "-?(\\d+\\.?\\d*)?")
        setPattern(cField, "-?(\\d+\\.?\\d*)?")
        setPattern(stepField, "(\\d+\\.?\\d*)?")

        xColumn.cellValueFactory = PropertyValueFactory<CoordinatesTable, Double>("xCoord")
        y1Column.cellValueFactory = PropertyValueFactory<CoordinatesTable, Double>("y1Coord")
        y2Column.cellValueFactory = PropertyValueFactory<CoordinatesTable, Double>("y2Coord")

        clearing()
    }

    private fun setPattern(textField: TextField, pattern: String) {
        val p = Pattern.compile(pattern)
        textField.textProperty().addListener { _, oldValue, newValue ->
            if (!p.matcher(newValue).matches()) textField.text = oldValue
        }
    }

    fun onClickCalculate() {

        clearing()
        val noErrors = checker()
        if (noErrors != CANCEL) {
            var count: Int
            stepField.text.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().also {
                count = if (it.isNotEmpty() && it.size > 1) {
                    it[1].length
                } else {
                    0
                }
            }

            var loader: FXMLLoader? = FXMLLoader()
            var loadStage: Stage? = Stage()
            loader?.location = Main::class.java.getResource("Views/LoadingLayout.fxml")
            val loadLayout = loader?.load<Any>() as AnchorPane
            loadStage?.initStyle(StageStyle.UNDECORATED)
            mainPane.isDisable = true

            var scene: Scene? = Scene(loadLayout)
            loadStage?.scene = scene
            loadStage?.sizeToScene()
            loadStage?.show()

            val array: ArrayList<XYChart.Series<Number, Number>> = runBlocking {
                calculatePoints(count)
            }

            GlobalScope.launch(Dispatchers.Main) {

                if (postErrors(array[0].data.size, noErrors)) {
                    chart2dTemp!!.data.addAll(array)
                    chart2dTemp!!.isLegendVisible = false
                    for (i in 0 until array.size) {
                        if (i == 0 || i == 2)
                            for (a in array[i].data) {
                                val x = String.format("%.${count}f", a.xValue).replace(",", ".").toDouble()
                                val y = String.format("%.${count}f", a.yValue).replace(",", ".").toDouble()
                                coordList?.add(CoordinatesTable(x, y, -y))
                            }
                    }

                    showingTooltip(chart2dTemp!!, count)
                    delay(600)
                    loadStage?.close()
                    loadStage = null
                    scene = null
                    loader = null

                    mainPane.isDisable = false
                    coordinatesView.items = coordList
                } else {
                    loadStage?.close()
                    loadStage = null
                    scene = null
                    loader = null

                    mainPane.isDisable = false
                    coordinatesView.items = coordList
                }
            }
        }
    }

    private fun postErrors(size: Int, errors: Errors): Boolean {
        val xFrom = xFromField.text.replace(",", ".").toDouble()
        val xTo = xToField.text.replace(",", ".").toDouble()

        return if (size > 1) {
            true
        } else if (size <= 1 && errors == OK) {
            val option: Optional<ButtonType> =
                showConfirmationAlert(
                    AlertType.CONFIRMATION,
                    "График может вырождаться в точку или не может быть построен",
                    "Продолжить ?"
                )
                    .showAndWait()
            return if (option.get() == ButtonType.OK) {
                xAxis =
                    NumberAxis(xFrom, xTo, (xTo - xFrom) / 10)
                yAxis =
                    NumberAxis(xFrom, xTo, (xTo - xFrom) / 10)
                chart2dTemp = LineChart(xAxis, yAxis)
                graphPane.children.clear()
                graphPane.children.add(chart2dTemp)
                true
            } else {
                false
            }
        } else if (size <= 1) {
            xAxis =
                NumberAxis(xFrom, xTo, (xTo - xFrom) / 10)
            yAxis =
                NumberAxis(xFrom, xTo, (xTo - xFrom) / 10)
            chart2dTemp = LineChart(xAxis, yAxis)
            graphPane.children.clear()
            graphPane.children.add(chart2dTemp)
            return true
        } else {
            true
        }
    }

    private fun checker(): Errors {
        val xFrom = xFromField.text.replace(",", ".").toDouble()
        val xTo = xToField.text.replace(",", ".").toDouble()
        val step = stepField.text.replace(",", ".").toDouble()
        val a = aField.text.replace(",", ".").toDouble()

        return if (xFrom <= xTo && step <= (xTo - xFrom)) {
            if (a != 0.0 && 5 <= (xTo - xFrom) / step) {
                OK
            } else if (a == 0.0) {
                val option: Optional<ButtonType> =
                    showConfirmationAlert(
                        AlertType.CONFIRMATION,
                        "Коэффициент А равен 0.\nГрафик может выраждаться в точку",
                        "Продолжить ?"
                    )
                        .showAndWait()
                if (option.get() == ButtonType.OK) {
                    ZERO
                } else {
                    CANCEL
                }
            } else {
                val option: Optional<ButtonType> =
                    showConfirmationAlert(AlertType.CONFIRMATION, "Слишком большой шаг", "Продолжить ?")
                        .showAndWait()
                if (option.get() == ButtonType.OK) {
                    BIG_STEP
                } else {
                    CANCEL
                }
            }
        } else {
            showConfirmationAlert(
                AlertType.ERROR,
                "Невозможно построить график с данными параметрами",
                "Проверьте корректность введенных данных!"
            )
                .showAndWait()
            CANCEL
        }
    }

    private fun showConfirmationAlert(type: AlertType, header: String, content: String): Alert {
        val a = Alert(type)
        a.contentText = content
        a.headerText = header
        a.title = "Внимание"
        return a
    }

    private suspend fun calculatePoints(count: Int): ArrayList<XYChart.Series<Number, Number>> =
        withContext(Dispatchers.Default) {
            var xFrom = xFromField.text.replace(",", ".").toDouble()
            val xTo = xToField.text.replace(",", ".").toDouble()
            val step = stepField.text.replace(",", ".").toDouble()
            val a = aField.text.replace(",", ".").toDouble()
            val c = cField.text.replace(",", ".").toDouble()
            val arrayY = ArrayList<Double>()
            var newGraph = false

            val seria1 = XYChart.Series<Number, Number>()
            val seria2 = XYChart.Series<Number, Number>()
            val seria3 = XYChart.Series<Number, Number>()
            val seria4 = XYChart.Series<Number, Number>()

            while (xFrom <= xTo) {
                val seriaY = Math.sqrt(Math.pow(a, 4.0) + (4 * Math.pow(c, 2.0) * Math.pow(xFrom, 2.0))) - Math.pow(
                    xFrom,
                    2.0
                ) - Math.pow(c, 2.0)
                if (!newGraph) {
                    if (seriaY >= 0.0) {
                        seria1.data.add(XYChart.Data(xFrom, Math.sqrt(seriaY)))
                        seria2.data.add(XYChart.Data(xFrom, -Math.sqrt(seriaY)))
                    } else if (arrayY.isNotEmpty() && arrayY.last() >= 0 && seriaY < 0.0) {
                        newGraph = true
                    }
                } else {
                    if (seriaY >= 0.0) {
                        seria3.data.add(XYChart.Data(xFrom, Math.sqrt(seriaY)))
                        seria4.data.add(XYChart.Data(xFrom, -Math.sqrt(seriaY)))
                    }
                }
                xFrom += step
                xFrom = String.format("%.${count}f", xFrom).replace(",", ".").toDouble()
                arrayY.add(seriaY)
            }

            if (seria3.data.size > 0) {
                return@withContext arrayListOf(seria1, seria2, seria3, seria4)
            } else {
                return@withContext arrayListOf(seria1, seria2)
            }
        }

    private fun showingTooltip(lineChart: LineChart<Number, Number>, count: Int) {
        for (i in lineChart.data) {
            val dataList = (i as XYChart.Series<*, *>).data
            dataList.forEach { data ->
                val node = data.node
                val x = String.format("%.${count}f", data.xValue)
                val y = String.format("%.${count}f", data.yValue)
                val tooltip =
                    Tooltip("($x;$y)")

                val fieldBehavior = tooltip.javaClass.getDeclaredField("BEHAVIOR")
                fieldBehavior.isAccessible = true
                val objBehavior = fieldBehavior.get(tooltip)
                val fieldTimer = objBehavior.javaClass.getDeclaredField("activationTimer")
                fieldTimer.isAccessible = true

                val objTimeline = fieldTimer.get(objBehavior) as Timeline
                objTimeline.keyFrames.clear()
                objTimeline.keyFrames.add(KeyFrame(Duration(150.0)))

                Tooltip.install(node, tooltip)
            }
        }

    }

    private fun clearing() {
        chart2dTemp?.data?.clear()
        chart2dTemp = null
        xAxis = null
        yAxis = null
        xAxis = NumberAxis()
        yAxis = NumberAxis()
        chart2dTemp = LineChart(xAxis, yAxis)
        graphPane.children.clear()
        graphPane.children.add(chart2dTemp)
        coordList = FXCollections.observableArrayList<CoordinatesTable>()
    }
}

data class CoordinatesTable(var xCoord: Double, var y1Coord: Double, var y2Coord: Double)
