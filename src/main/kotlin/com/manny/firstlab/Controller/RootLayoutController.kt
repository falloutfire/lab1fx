package com.manny.firstlab.Controller

import com.manny.firstlab.Main
import javafx.scene.control.Alert
import javafx.stage.FileChooser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import java.io.File
import java.io.FileOutputStream


class RootLayoutController {

    private var main: Main? = null

    fun setMainApp(mainApp: Main) {
        this.main = mainApp
    }

    fun handleExport() {
        val fileChooser = FileChooser()
        val extensionFilter = FileChooser.ExtensionFilter(
            "XLS files (*.xls)", "*.xls"
        )
        fileChooser.extensionFilters.add(extensionFilter)

        var file: File? = fileChooser.showSaveDialog(main?.primaryStage)
        if (file != null) {
            if (!file.path.endsWith(".xls")) {
                file = File(file.path + ".xls")

            }
        }

        if (runBlocking { exportDataToFile(file) }) {
            val alert = Alert(Alert.AlertType.INFORMATION)
            alert.title = "Экспорт"
            alert.headerText = "Экспорт завершен"
            alert.contentText = "Результаты сохранены в отчет:\n" + file?.path

            alert.showAndWait()
        } else {
            val alert = Alert(Alert.AlertType.ERROR)
            alert.title = "Ошибка"
            alert.headerText = "Експорт отменен"
            alert.contentText = "Файл не был сохранен"

            alert.showAndWait()
        }
    }

    private suspend fun exportDataToFile(file: File?): Boolean =
        withContext(Dispatchers.IO) {
            val book = HSSFWorkbook()
            val sheet = book.createSheet("Расчет")

            val title = sheet.createRow(0).createCell(0)
            title.setCellValue("Исходные данные")

            val titleRow = sheet.createRow(1)
            val xFrom = titleRow.createCell(0)
            xFrom.setCellValue("Начало интервала")
            val xTo = titleRow.createCell(1)
            xTo.setCellValue("Конец интервала")
            val a = titleRow.createCell(2)
            a.setCellValue("А")
            val c = titleRow.createCell(3)
            c.setCellValue("С")
            val step = titleRow.createCell(4)
            step.setCellValue("Шаг")

            val valRow = sheet.createRow(2)
            val xFromV = valRow.createCell(0)
            xFromV.setCellValue(main?.mainController?.xFromField?.text!!.toDouble())
            val xToV = valRow.createCell(1)
            xToV.setCellValue(main?.mainController?.xToField?.text!!.toDouble())
            val aV = valRow.createCell(2)
            aV.setCellValue(main?.mainController?.aField?.text!!.toDouble())
            val cV = valRow.createCell(3)
            cV.setCellValue(main?.mainController?.cField?.text!!.toDouble())
            val stepV = valRow.createCell(4)
            stepV.setCellValue(main?.mainController?.stepField?.text!!.toDouble())

            val titleRowValue = sheet.createRow(4)
            val x = titleRowValue.createCell(0)
            x.setCellValue("X")
            val y1 = titleRowValue.createCell(1)
            y1.setCellValue("Y1")
            val y2 = titleRowValue.createCell(2)
            y2.setCellValue("Y2")

            val chart = main?.mainController?.coordList
            var counter = 5
            for (i in chart!!) {
                val rowValue = sheet.createRow(counter)
                val xV = rowValue.createCell(0)
                xV.setCellValue(i.xCoord)
                val y1V = rowValue.createCell(1)
                y1V.setCellValue(i.y1Coord)
                val y2V = rowValue.createCell(2)
                y2V.setCellValue(i.y2Coord)
                counter++
            }

            sheet.autoSizeColumn(1)
            try {
                book.write(FileOutputStream(file))
                book.close()
                true
            } catch (e: Exception) {
                e.printStackTrace()
                false
            }
        }

    fun handleAbout() {
        val alert = Alert(Alert.AlertType.INFORMATION)
        alert.title = "Information Systems and Technology"
        alert.headerText = "О программе"
        alert.contentText = "Авторы: Илья Лихачев, Илья Родионов,\nВладислав Яшин\nГруппа 455"

        alert.showAndWait()
    }
}