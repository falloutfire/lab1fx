package com.manny.firstlab

import com.manny.firstlab.Controller.MainController
import com.manny.firstlab.Controller.RootLayoutController
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import java.io.IOException

class Main : Application() {

    var primaryStage: Stage? = null
        private set
    var mainController: MainController? = null
        private set
    private var rootLayout: BorderPane? = null

    override fun start(primaryStage: Stage?) {
        this.primaryStage = primaryStage
        this.primaryStage!!.title = "Овал Кассини"
        this.primaryStage!!.isResizable = false
        this.primaryStage!!.sizeToScene()
        initRootLayout()
        showMainLayout()
    }

    @Throws(IOException::class)
    private fun showMainLayout() {
        try {
            val loader = FXMLLoader()
            loader.location = Main::class.java.getResource("Views/MainLayout.fxml")
            val lruPane = loader.load<AnchorPane>()

            rootLayout!!.center = lruPane

            mainController = loader.getController()

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @Throws(IOException::class)
    private fun initRootLayout() {
        try {
            val loader = FXMLLoader()
            loader.location = Main::class.java.getResource("Views/RootLayout.fxml")
            rootLayout = loader.load<Any>() as BorderPane

            val scene = Scene(rootLayout!!)
            primaryStage!!.scene = scene
            primaryStage!!.show()

            val rootLayoutController = loader.getController<RootLayoutController>()
            rootLayoutController.setMainApp(this)

        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

}

fun main() {
    Application.launch(Main::class.java)
}
